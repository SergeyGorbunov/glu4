package ru.gameislife.glupingpong;

import ru.gameislife.glupingpong.service.AppServiceFactory;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 7/3/2016
 * Time: 1:34 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class App {
    public static void main(String[] args) {
        AppServiceFactory
                .getService(args)
                .ifPresent(AppService::run);
    }
}
