package ru.gameislife.glupingpong.command.response;

import ru.gameislife.glupingpong.command.Response;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 7/4/2016
 * Time: 4:51 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class PongResponse implements Response {

    private int userId;
    private int pong;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPong() {
        return pong;
    }

    public void setPong(int pong) {
        this.pong = pong;
    }

    @Override
    public String toString() {
        return "PongResponse{" +
                "userId=" + userId +
                ", pong=" + pong +
                '}';
    }
}
