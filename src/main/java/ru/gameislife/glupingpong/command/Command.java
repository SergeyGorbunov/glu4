package ru.gameislife.glupingpong.command;

import ru.gameislife.glupingpong.storage.Storage;
import ru.gameislife.glupingpong.storage.entity.User;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 7/5/2016
 * Time: 12:17 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface Command {

    User getUser();

    int getUserId();

    void setUserId(int userId);

    void preAction();

    void action();

    void postAction();

    Response getResponse();

    void setResponse(Response response);

    Storage getStorage();

    void setStorage(Storage storage);
}
