package ru.gameislife.glupingpong.command.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.gameislife.glupingpong.command.response.PongResponse;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 7/3/2016
 * Time: 8:53 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class PingCommand extends BaseCommand {
    private static final Logger LOG = LoggerFactory.getLogger(PingCommand.class);

    @Override
    public void action() {
        PongResponse response = new PongResponse();
        response.setUserId(getUserId());
        response.setPong(getUser().addAndGet());
        setResponse(response);
    }

    @Override
    public String toString() {
        return "PingCommand{} " + super.toString();
    }
}
