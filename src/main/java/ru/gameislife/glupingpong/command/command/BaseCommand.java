package ru.gameislife.glupingpong.command.command;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.gameislife.glupingpong.command.Command;
import ru.gameislife.glupingpong.command.Response;
import ru.gameislife.glupingpong.storage.Storage;
import ru.gameislife.glupingpong.storage.entity.User;
import ru.gameislife.glupingpong.storage.repository.UserRepository;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 7/3/2016
 * Time: 9:51 PM
 * To change this template use File | Settings | File and Code Templates.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "COMMAND"
)
@JsonSubTypes({

        // It's need by task
        @JsonSubTypes.Type(value = PingCommand.class, name = "PING"),

        // It's possible commands
        // @JsonSubTypes.Type(value = LoginCommand.class, name = "LOGIN"),
        // @JsonSubTypes.Type(value = LogoutCommand.class, name = "LOGOUT"),
})
public abstract class BaseCommand implements Command {
    private static final Logger LOG = LoggerFactory.getLogger(BaseCommand.class);

    private User user;
    private int userId;
    private Storage storage;
    private Response response;

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public int getUserId() {
        return userId;
    }

    @Override
    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public void preAction() {
        UserRepository repository = storage.getUserRepository();
        if (!repository.exist(userId)) {

            // new user
            LOG.debug("Create and add user, userId {}", userId);
            user = createUser();
            repository.add(user);

        } else {

            // exist user
            LOG.debug("Get user, userId {}", userId);
            user = repository.get(userId);
        }
    }

    private User createUser() {
        User user = new User();
        user.setUserId(userId);
        user.setPong(0);
        return user;
    }

    @Override
    public void postAction() {
        UserRepository repository = storage.getUserRepository();

        // save changes
        LOG.debug("Edit user {}", user);
        repository.edit(user);
    }

    @Override
    public Response getResponse() {
        return response;
    }

    @Override
    public void setResponse(Response response) {
        this.response = response;
    }

    @Override
    public Storage getStorage() {
        return storage;
    }

    @Override
    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    @Override
    public String toString() {
        return "BaseCommand{" +
                "userId=" + userId +
                '}';
    }
}
