package ru.gameislife.glupingpong;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 7/3/2016
 * Time: 1:58 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface AppService {
    void run();
}
