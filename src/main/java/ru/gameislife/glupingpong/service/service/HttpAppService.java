package ru.gameislife.glupingpong.service.service;

import akka.actor.ActorSystem;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.IncomingConnection;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.model.MediaTypes;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.gameislife.glupingpong.AppService;
import ru.gameislife.glupingpong.command.Response;
import ru.gameislife.glupingpong.command.command.BaseCommand;
import ru.gameislife.glupingpong.storage.Storage;
import ru.gameislife.glupingpong.storage.storage.CassandraStorage;

import java.util.concurrent.CompletionStage;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 7/3/2016
 * Time: 2:05 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class HttpAppService implements AppService {
    private static final Logger LOG = LoggerFactory.getLogger(HttpAppService.class);

    private Storage storage;
    private String host;
    private int port;

    public HttpAppService(String host, int port) {
        this.storage = new CassandraStorage();
        this.host = host;
        this.port = port;
    }

    @Override
    public void run() {
        ActorSystem system = ActorSystem.create("http", ConfigFactory.load("http.conf"));
        Materializer materializer = ActorMaterializer.create(system);

        // connect
        ConnectHttp connect = ConnectHttp.toHost(host, port);

        // bind
        Source<IncomingConnection, CompletionStage<ServerBinding>> source = Http
                .get(system)
                .bind(connect, materializer);

        // map
        source.to(Sink.foreach(connection -> connection
                .handleWithAsyncHandler(httpRequest -> {

                    // parse && action
                    CompletionStage<Response> response = Jackson
                            .unmarshaller(BaseCommand.class)
                            .unmarshall(
                                    httpRequest.entity(),
                                    materializer.executionContext(),
                                    materializer)
                            .thenApply(command -> {

                                // TODO add user session lock
                                command.setStorage(storage);
                                command.preAction();
                                command.action();
                                command.postAction();

                                // response
                                return command.getResponse();
                            });

                    // answer
                    // TODO use Jackson.marshaller
                    return response
                            .thenApply(answer -> HttpResponse
                                    .create()
                                    .withEntity(
                                            MediaTypes.APPLICATION_JSON.toContentType(),
                                            HttpAppService.this.toJson(answer)));
                }, materializer)))
                .run(materializer);
    }

    private byte[] toJson(Response response) {
        try {
            return new ObjectMapper().writeValueAsBytes(response);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Cannot write to JSON", e);
        }
    }
}
