package ru.gameislife.glupingpong.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.gameislife.glupingpong.AppService;
import ru.gameislife.glupingpong.service.service.HttpAppService;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 7/3/2016
 * Time: 2:05 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class AppServiceFactory {
    private static final Logger LOG = LoggerFactory.getLogger(AppServiceFactory.class);

    public static Optional<AppService> getService(String[] args) {

        // service name
        String name = args[0];

        // finding
        if (name.equals("http")) {
            String host = args[1].trim();
            int port = Integer.parseInt(args[2].trim());
            return Optional.of(new HttpAppService(host, port));
        }

        // not found
        LOG.warn("Service not found, name {}", name);
        return Optional.empty();
    }
}
