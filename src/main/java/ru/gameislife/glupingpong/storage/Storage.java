package ru.gameislife.glupingpong.storage;

import ru.gameislife.glupingpong.storage.repository.UserRepository;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 7/4/2016
 * Time: 6:08 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface Storage {
    UserRepository getUserRepository();
}
