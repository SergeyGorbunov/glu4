package ru.gameislife.glupingpong.storage.entity;

import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 7/3/2016
 * Time: 9:02 PM
 * To change this template use File | Settings | File and Code Templates.
 */
@Table
public class User {

    @PrimaryKey
    private int userId;

    private int pong;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPong() {
        return pong;
    }

    public void setPong(int pong) {
        this.pong = pong;
    }

    public int addAndGet() {
        return ++pong;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", pong=" + pong +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return userId == user.userId;
    }

    @Override
    public int hashCode() {
        return userId;
    }
}
