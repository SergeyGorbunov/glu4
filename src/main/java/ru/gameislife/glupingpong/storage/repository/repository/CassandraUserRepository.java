package ru.gameislife.glupingpong.storage.repository.repository;

import com.datastax.driver.core.querybuilder.Clause;
import com.datastax.driver.core.querybuilder.Delete;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.cassandra.core.CassandraOperations;
import ru.gameislife.glupingpong.storage.entity.User;
import ru.gameislife.glupingpong.storage.repository.UserRepository;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 7/4/2016
 * Time: 5:35 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class CassandraUserRepository implements UserRepository {
    private static final Logger LOG = LoggerFactory.getLogger(CassandraUserRepository.class);

    private CassandraOperations operations;

    public CassandraUserRepository(CassandraOperations operations) {
        this.operations = operations;
    }

    @Override
    public User get(int userId) {
        Select select = QueryBuilder.select().from("User");
        select.where(byUserId(userId));
        return operations.selectOne(select, User.class);
    }

    @Override
    public void add(User user) {
        operations.insert(user);
    }

    @Override
    public void edit(User user) {
        operations.update(user);
    }

    @Override
    public void delete(int userId) {
        Delete delete = QueryBuilder.delete().from("User");
        delete.where(byUserId(userId));
        operations.execute(delete);
    }

    @Override
    public boolean exist(int userId) {
        Select select = QueryBuilder.select().from("User");
        select.where(byUserId(userId));
        return operations
                .select(select, User.class)
                .size() > 0;
    }

    private Clause byUserId(int userId) {
        return QueryBuilder.eq("userId", userId);
    }
}
