package ru.gameislife.glupingpong.storage.repository;

import ru.gameislife.glupingpong.storage.entity.User;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 7/4/2016
 * Time: 4:49 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface UserRepository {

    User get(int userId);

    void add(User user);

    void edit(User user);

    void delete(int userId);

    boolean exist(int userId);
}
