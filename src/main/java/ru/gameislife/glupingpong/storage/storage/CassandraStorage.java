package ru.gameislife.glupingpong.storage.storage;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.CassandraTemplate;
import ru.gameislife.glupingpong.storage.Storage;
import ru.gameislife.glupingpong.storage.repository.UserRepository;
import ru.gameislife.glupingpong.storage.repository.repository.CassandraUserRepository;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 7/4/2016
 * Time: 9:56 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class CassandraStorage implements Storage {
    private static final Logger LOG = LoggerFactory.getLogger(CassandraStorage.class);

    private CassandraOperations operations;

    public CassandraStorage() {

        // init cluster
        Cluster cluster = Cluster
                .builder()
                .addContactPoint("127.0.0.1")
                .build();

        // connect
        Session session = cluster.connect("glu");
        operations = new CassandraTemplate(session);
    }

    @Override
    public UserRepository getUserRepository() {
        return new CassandraUserRepository(operations);
    }
}
