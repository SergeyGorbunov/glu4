```
#!cassandra

CREATE KEYSPACE glu WITH REPLICATION = {'class':'SimpleStrategy', 'replication_factor':1};

USE glu;

CREATE TABLE users (userId int PRIMARY KEY, pong int);
```